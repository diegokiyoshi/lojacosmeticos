2. DESENVOLVER UM SOFTWARE PARA UMA LOJA DE COSMÉTICOS, ONDE:
	
	- o cliente cadastra seus produtos, com preço, marca, tipo de produto, quantidade de estoque;
	- o cliente consulte, edite o cadastro, exclua e vende.  
	- o sistema também tem a opção de emitir um relatório sobre os produtos vendidos, ou enviar o relatório por email.
	- cadastrar clientes da loja, onde terão informações como: nome, endereço, cpf, data de nascimento e cep.
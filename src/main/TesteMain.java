package main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.cosmeticosabc.Cliente;
import br.com.cosmeticosabc.LojaCosmeticos;

public class TesteMain {
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws ParseException {
		Cliente cliente1 = new Cliente();
		LojaCosmeticos loja1 = new LojaCosmeticos();
		Date dataDeAgora = new Date();
		SimpleDateFormat dataFormatadaComBarras = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat dataFormatadaHorasMinutosSegundos = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat dataFormatadaComPontos = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		
//		cliente1.setDataHoraChegada(dataFormatadaHorasMinutosSegundos.parse("10/01/2019 11:55:55"));
//		System.out.println("DATA E HORA DE CHEGADA:" + cliente1.getDataHoraChegada());

//		Calendar calendar = Calendar.getInstance();
//		//calendar.setTime(dataFormatadaComBarras.parse("03/02/1998"));
//		
//		calendar.set(Calendar.DAY_OF_MONTH, 12);	
//		calendar.set(Calendar.MONTH, 0);
//		calendar.set(Calendar.YEAR, 1998);
//		System.out.println("TESTE: " + calendar.getTime() + "\n");
//		
//		
//		calendar.add(Calendar.DAY_OF_MONTH, 29);
//		calendar.add(Calendar.YEAR, 20);
//		calendar.add(Calendar.MONTH, 11);
//		calendar.add(Calendar.HOUR, 11);
//
//		Date dataModificada = calendar.getTime();
//		
//		System.out.println(dataModificada);
//		System.out.println("Calendar "+dataFormatadaHorasMinutosSegundos.format(calendar.getTime()));

		
		/*
		 * cliente1.setCep("03518-600"); cliente1.setCpf("226787878700");
		 * cliente1.setEndereco("Rua Boa Vista, 123"); cliente1.setNome("Joao");
		 */

//		dataDeAgora.setDate(-365);
//		
//		System.out.println("Data de agora: Usando o getDate " + dataDeAgora.getDate());
//		
		System.out.println("Convertendo data SimpleDateFormat em formato Date: "
		+ dataFormatadaComBarras.parse("18/04/2013"));
		
		System.out.println("Convertendo Date em SimpleDateFormat: " + 
		dataFormatadaComBarras.format(dataDeAgora));
		
		System.out.println(dataDeAgora);
		
		String dataFormatadaComBarrasFormat = dataFormatadaComBarras.format(dataDeAgora);
		cliente1.setDataNascimento(dataFormatadaComBarras.parse(dataFormatadaComBarrasFormat));
		System.out.println("Data de nascimento: " + cliente1.getDataNascimento());
		
		
		cliente1.setDataHoraChegada(dataFormatadaHorasMinutosSegundos.parse("20/12/2016 16:53:20"));
		System.out.println("DATA E HORA DE CHEGADA:" + cliente1.getDataHoraChegada());
		
		cliente1.setDataHoraSaida(dataFormatadaComPontos.parse("2019.01.10 13:42:50"));
		System.out.println("DATA E HORA DE SAIDA: " + cliente1.getDataHoraSaida());
//		//loja1.cadastrarCliente(cliente1);
		
		

	}

}
